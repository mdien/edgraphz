
##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

from PyQt5.QtCore import QRectF

from model.convertDOT import ConvertDOT
from view.graphicsnode import *
from view.graphicstextitem import *
from view.graphicsedge import *

class MainView(QGraphicsView):
    """
    A custom QGraphicsView

    """

    def __init__(self, parent):
        """
        Constructor

        @param parent: the widget who own this MainView

        """
        QGraphicsView.__init__(self,parent)
        self.setDragMode(QGraphicsView.RubberBandDrag)
        
        self.setSceneRect(QRectF(-3000.,-3000.,6000.,6000.))


        #Makes all more smooth (line, circle, ...) but more laggy sometimes


        #set the "center" of the transformation. Here it's NoAnchor because we will make it ourself
        self.setTransformationAnchor(QGraphicsView.NoAnchor)
        
        self.hScrollBar = self.horizontalScrollBar()
        self.vScrollBar = self.verticalScrollBar()

        #hide both horizontal and vertical scroll bars
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

    def wheelEvent(self, event):
        """
        This method is called when a wheelEvent occurs

        @param event: the wheel event
        
        """
        self.zoom(event.angleDelta().y()/100.0)

    def zoom(self, factor):
        """
        Allow the user to zoom in and out when in the view

        @param factor: the zoom factor

        """

        if factor < 0.:
            factor = -1./factor
        posView1 = self.mapFromGlobal(QCursor.pos())
        posScene = self.mapToScene(posView1)
        self.scale(factor, factor)
        posView2 = self.mapFromScene(posScene)
        dxView = posView2.x() - posView1.x()
        dyView = posView2.y() - posView1.y()
        self.hScrollBar.setValue(self.hScrollBar.value()+dxView)
        self.vScrollBar.setValue(self.vScrollBar.value()+dyView)


    
    def contextMenuEvent(self, e):
        """
        create a context menu when the view is right-clicked

        @param e: the mouse event

        """

        menu = QMenu()
        item = self.itemAt(e.pos())

        contains_edge= False 
        contains_node = False

        for items in self.scene().selectedItems():
            if hasattr(items, 'parent'):
                items = items.parent
            if isinstance(items,(GraphicsNode,SquareNode, DiamondNode, TriangleNode)) :
                contains_node = True
            else :
                contains_edge = True


        act1 = QAction(self)
        act1.setSeparator(True)
        act2 = QAction(self)
        act2.setSeparator(True)
        
        act3 = QAction(self)
        act3.setSeparator(True)
        act4 = QAction(self)
        act4.setSeparator(True)
        act5 = QAction(self)
        act5.setSeparator(True)
        
        changeColorNode = menu.addAction("Change nodes color")
        changeShapeNode = menu.addAction("Change nodes shape")
        changeColorNode.setEnabled(False)
        changeShapeNode.setEnabled(False)
        menu.addAction(act1)
        changeColorEdge = menu.addAction("Change edges color")
        changeStyleEdge = menu.addAction("Change edges style")
        changeColorEdge.setEnabled(False)
        changeStyleEdge.setEnabled(False)
        menu.addAction(act2)
        makeClique = menu.addAction("Make a clique")
        makeClique.setEnabled(False)
        menu.addAction(act3)
        if contains_node:
            changeColorNode.setEnabled(True)
            changeShapeNode.setEnabled(True)
            makeClique.setEnabled(True)
        if contains_edge:
            changeColorEdge.setEnabled(True)
            changeStyleEdge.setEnabled(True)
        selectAll = menu.addAction("Select all")
        menu.addAction(act4)
        exportAllAct = menu.addAction("Export all")
        menu.addAction(act5)
        importAct = menu.addAction("Import")
        
        action = menu.exec_(self.mapToGlobal(e.pos()))

        #if the user make a choice among the actions
        if action != None:

            if action == selectAll:
                for item in self.scene().items():
                    item.setSelected(True)

            elif action == exportAllAct:
                print("export")
                convert = ConvertDOT(self.parent().parent(), self.scene().editor)
                convert.write_dot()

            elif action == importAct:
                print("import")

            #if the user have right-clicked on an item (node, edge, label, etc)
            
            elif item != None:
                if action == changeColorNode:
                    selectedItems = [item for item in self.scene().selectedItems() if isinstance(item,(GraphicsNode,SquareNode, DiamondNode, TriangleNode))]
                    initColor = selectedItems[0].brush().color()
                    color = QColorDialog.getColor(initColor, self, 'change nodes color')
                    if color.isValid():
                        pen = QBrush(color)
                        for item in selectedItems:
                            item.setBrush(pen)
                            self.scene().map_to_scene[item].color = pen
                    self.scene().refresh_main_window()

                elif action == changeShapeNode:
                    selectedItems = [item for item in self.scene().selectedItems() if isinstance(item,(GraphicsNode,SquareNode, DiamondNode, TriangleNode))]
                    shapes = ("ellipse", "square", "diamond", "triangle")
                    newShape, okPressed = QInputDialog.getItem(self,"New Shape", "Shape:", shapes, 0, False)
                    if okPressed:
                        for item in selectedItems:
                            self.scene().map_to_scene[item].form = newShape
                    for item in self.scene().items():
                        item.setFlag(QGraphicsItem.ItemIsMovable, False)
                    self.scene().refresh_main_window()

                elif action == changeColorEdge:
                    selectedItems = [item for item in self.scene().selectedItems() if isinstance(item,(GraphicsEdge,GraphicsSelfEdge))]
                    color = QColorDialog.getColor(Qt.black, self, 'change the color')
                    if color.isValid():
                        for item in selectedItems:
                            self.scene().map_to_scene[item].color = color
                    self.scene().refresh_main_window()

                elif action == changeStyleEdge:
                    selectedItems = [item for item in self.scene().selectedItems() if isinstance(item,(GraphicsEdge, GraphicsSelfEdge))]
                    lineStyles = ("solid", "dashed", "dotted", "bold")
                    newStyle, okPressed = QInputDialog.getItem(self,"New Line Style", "Style:", lineStyles, 0, False)
                    if okPressed:
                        for item in selectedItems:
                            self.scene().map_to_scene[item].form = newStyle
                    self.scene().refresh_main_window()                        

                elif action == makeClique:
                    selectedNodes = [item for item in self.scene().selectedItems() if isinstance(item,(GraphicsNode,SquareNode, DiamondNode, TriangleNode))]
                    selectedEdges = [item for item in self.scene().selectedItems() if isinstance(item,GraphicsEdge)]
                    nextNode = 1
                    baseCpt = 1
                    cpt = 1
                    countNode = len(selectedNodes)
                    for edge in selectedEdges:
                        self.scene().editor.remove_edge_element(self.scene().map_to_scene[edge])
                    for node in selectedNodes:
                        baseCpt +=1
                        while cpt < countNode :
                            nodeA = self.scene().map_to_scene[node]
                            nodeB = self.scene().map_to_scene[selectedNodes[cpt]]
                            self.scene().editor.add_edge_element(self, nodeA, nodeB, False, "edge", QColor("#000000"),"solid", self.scene().editor.increment_num_id(), 0)
                            cpt+=1
                        cpt = baseCpt
                    self.scene().refresh_edge_window()

    
