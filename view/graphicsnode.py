##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

from view.graphicsedge import *
from view.graphicstextitem import GraphicsTextItem
from model.edge import *


class GraphicsMainNode(QGraphicsItem):

    def __init__(self, editor, map_to_scene, label):
        
        QGraphicsItem.__init__(self)
        self.editor = editor
        self.map_to_scene = map_to_scene
        self.text = label
        self.textItem = GraphicsTextItem(self.text, map=self.map_to_scene, editor=self.editor,
                                         parent=self)
            
        


    def mouseReleaseEvent(self, mouse_event):
        super().mouseReleaseEvent(mouse_event)

        nodes = self.map_to_scene
        node = nodes[self]
        Xx = node.position_x
        Yy = node.position_y
        coord = QPointF(Xx, Yy)
        
        move = self.scenePos() - coord

        for item in self.scene().selectedItems():
            current_node = nodes[item]
            current_node.position_x += move.x()
            current_node.position_y += move.y()

        if self.edgeInConstruction is not None:
            self.scene().removeItem(self.edgeInConstruction)
            self.edgeInConstruction = None
            items = [it for it in self.scene().items(mouse_event.scenePos()) if
                     (isinstance(it, (GraphicsNode, DiamondNode, SquareNode, TriangleNode)))]
            if len(items) != 0:
                node1 = self.map_to_scene[self]
                node2 = self.map_to_scene[items[0]]
                self.editor.add_edge_element(None, node1, node2, self.editor.is_oriented, "edge", QColor("#000000"), "solid", self.editor.increment_num_id(), None)
                self.scene().refresh_main_window()

    def mouseMoveEvent(self, mouse_event):
        """
        This method is called whenever the user move the mouse

        :param mouse_event: the mouse event
        """
        super().mouseMoveEvent(mouse_event)
        modifiers = QApplication.keyboardModifiers()
        if modifiers is not Qt.ControlModifier:
            if self.edgeInConstruction is not None:
                self.edgeInConstruction.Update(mouse_event.scenePos())
        self.scene().refresh_edge_window()

    def buildEdge(self, pos):
        """
        This method build a semi-edge between the node and the cursor position

        :param pos: the other extremity of the semi-edge (must be the cursor position)

        """
        self.edgeInConstruction = GraphicsSemiEdge(self, pos, self.editor)
        self.scene().addItem(self.edgeInConstruction)

    def mousePressEvent(self, mouse_event):
        """
        This method is called when the user press a mouse button

        :param mouse_event: the mouse event
        """
        modifiers = QApplication.keyboardModifiers()
        selectedItems = self.scene().selectedItems()
        if self not in selectedItems:
            for item in selectedItems:
                item.setSelected(False)
            self.setSelected(True)
        if modifiers == Qt.NoModifier and mouse_event.button() != Qt.RightButton:
            self.setSelected(True)
            if self.editor.type_action == "edge":
                self.buildEdge(mouse_event.scenePos())
        elif mouse_event.button() == Qt.RightButton:
            self.scene().removeItem(self.edgeInConstruction)
            self.edgeInConstruction = None
        self.scene().refresh_edge_window()


class GraphicsNode(QGraphicsEllipseItem, GraphicsMainNode):
    """
    A custom QGraphicsEllipseItem. Used when drawing an ellipse-shaped node

    """

    def __init__(self,editor, map_to_scene, label):
        """
        Constructor

        :param current_state: a reference to the model
        :param pair_model_node: a dictionary linking the view and the model
        :param label: the current name of this node

        """
        super().__init__(editor, map_to_scene, label)
        self.textItem.setTextInteractionFlags(Qt.TextEditorInteraction)
        self.setRect(self.textItem.boundingRect().marginsAdded(QMarginsF(10, 10, 10, 10)))
        self.setFlags(QGraphicsItem.ItemIsSelectable)
        self.edgeInConstruction = None
        



###############################################################################
###############################################################################
class SquareNode(QGraphicsRectItem, GraphicsMainNode):
    """
    A custom QGraphicsRectItem. Used when drawing a rectangular node

    """

    def __init__(self, editor, map_to_scene, label):
        """
        Constructor

        :param current_state: a reference to the model
        :param pair_model_node: a dictionary linking the view and the model
        :param label: the current name of this node

        """
        super().__init__(editor, map_to_scene, label)

        self.textItem.setTextInteractionFlags(Qt.TextEditorInteraction)
        self.setRect(self.textItem.boundingRect().marginsAdded(QMarginsF(10, 10, 10, 10)))
        self.setFlags(QGraphicsItem.ItemIsSelectable)
        self.edgeInConstruction = None


###############################################################################
###############################################################################
class DiamondNode(QGraphicsPolygonItem, GraphicsMainNode):
    """
    A custom QGraphicsPolygonItem. Used when drawing a diamond-shaped node

    """

    def __init__(self, editor, map_to_scene, label):
        """
        Constructor

        :param current_state: a reference to the model
        :param pair_model_node: a dictionary linking the view and the model
        :param label: the current name of this node

        """
        super().__init__(editor, map_to_scene, label)
        textW = self.textItem.boundingRect().width()
        textH = self.textItem.boundingRect().height()
        pointA = QPointF(textW / 2, -10.)
        pointB = QPointF(textW + 10, textH / 2)
        pointC = QPointF(textW / 2, textH + 10)
        pointD = QPointF(-10., textH / 2)
        self.textItem.setTextInteractionFlags(Qt.TextEditorInteraction)
        self.setPolygon(QPolygonF( [pointA, pointB, pointC, pointD]))
        self.setFlags(QGraphicsItem.ItemIsSelectable)
        self.edgeInConstruction = None


###############################################################################
###############################################################################
class TriangleNode(QGraphicsPolygonItem, GraphicsMainNode):
    """
    A custom QGraphicsPolygonItem. Used when drawing a triangular node

    """

    def __init__(self, editor, map_to_scene, label):
        """
        Constructor

        :param current_state: a reference to the model
        :param pair_model_node: a dictionary linking the view and the model
        :param label: the current name of this node

        """
        super().__init__(editor, map_to_scene, label) 
        textW = self.textItem.boundingRect().width()
        textH = self.textItem.boundingRect().height()
        p1 = QPointF(-0.5 * textW, 1.5 * textH)
        p2 = QPointF(1.5 * textW, 1.5 * textH)
        p3 = QPointF(0.5 * textW, -1.5 * textH)
        self.textItem.setTextInteractionFlags(Qt.TextEditorInteraction)
        self.setPolygon(QPolygonF([p1, p2, p3]))
        self.setFlags(QGraphicsItem.ItemIsSelectable)
        self.edgeInConstruction = None

def graphicsnode(form, editor, map_to_scene, label):
    if form == "ellipse":
        return GraphicsNode(editor, map_to_scene, label)
    elif form == "square":
        return SquareNode(editor, map_to_scene, label)
    elif form == "triangle":
        return TriangleNode(editor, map_to_scene, label)
    elif form == "diamond":
        return DiamondNode(editor, map_to_scene, label)
    else:
        return
