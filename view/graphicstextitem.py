##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

from PyQt5.QtWidgets import QGraphicsTextItem

import pdb


class GraphicsTextItem(QGraphicsTextItem):
    """
    A custom QGraphicsTextItem
    It will be used as an Edge label

    """

    def __init__(self, *args, **kwargs):
        """
        Constructor

        :param *args: a list of parameters
        :param **kwargs: a dictionary of named parameters
        """

        super().__init__(*args, kwargs.get("parent"))
        self.text = args[0]
        self.parent = kwargs.get("parent")
        self.map = kwargs.get("map")
        self.editor = kwargs.get("editor")

    def keyPressEvent(self, mouse_event):
        """
        Called when the user type something on the keyboard

        :param mouse_event: the keyboard event
        """
        super().keyPressEvent(mouse_event)

        self.map[self.parent].label = self.document().toPlainText()

    def mousePressEvent(self, e):
        """
        Called when the user click the mouse

        :param e: the mouse event

        """
        super().mousePressEvent(e)



    def mouseReleaseEvent(self, e):
        """
        Called when the user release the click

        :param e: the mouse event

        """
        self.parentItem().mouseReleaseEvent(e)

    def focusOutEvent(self, e):
        """
        When the object textItem no longer has the focus, the view is refresh

        :param e:
        """
        super().focusOutEvent(e)
        self.scene().refresh_main_window()
        