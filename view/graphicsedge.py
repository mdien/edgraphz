##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

from math import sqrt

from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt, QMarginsF, QPointF, QLineF
from PyQt5.QtGui import *
from view.graphicstextitem import *

def closestPointTo(point, path):
    """
    This function take a point and a path and return the point on the path which is closest to the point passed in argument 

    @param point: a point (must be out of the path)
    @param path: the path that is the shape of an object

    @return: a point (QPointF)
    """
    target = path.boundingRect().center()
    mid = (point + target) / 2.

    if path.contains(mid):
        return mid
    else:
        while (mid - point).manhattanLength() > 1:
            while not path.contains(mid):
                point = mid
                mid = (point + target) / 2.

            while path.contains(mid):
                mid = (point + mid) / 2.

        return mid

class GraphicsMainEdge(QGraphicsItem):

    def __init__(self, startNode, endNode, editor, map_to_scene, label, color, form):

        super().__init__()
        
        self.startNode = startNode
        self.endNode = endNode
        self.editor = editor
        self.map_to_scene = map_to_scene
        self.label = label
        self.color = color
        self.form = form

        startShape = self.startNode.mapToScene(self.startNode.shape())
        if isinstance(self.endNode, QPointF) :
            endShape = startShape
        else :
            endShape = self.endNode.mapToScene(self.endNode.shape())
        

        self.textItem = GraphicsTextItem(self.label, map=self.map_to_scene, editor=self.editor, parent=self)
        self.textItem.setTextInteractionFlags(Qt.TextEditorInteraction)

        if startNode != endNode :
            p1 = closestPointTo(endShape.boundingRect().center(), startShape)
            p2 = closestPointTo(startShape.boundingRect().center(), endShape)
            self.textItem.setPos(((p1.x() + p2.x()) / 2), (((p1.y() + p2.y()) / 2)))
            
            stX = p1.x()
            stY = p1.y()
            

        else :
            p1 = closestPointTo(startShape.boundingRect().center() - QPointF(50, 100), startShape)
            p2 = closestPointTo(startShape.boundingRect().center() - QPointF(-50, 100), startShape)
            mid = (p1 + p2) / 2

            sizeRect = self.textItem.boundingRect()
            self.textItem.setPos(mid.x() - sizeRect.width() / 2, mid.y() - 50)

            stX = startShape.boundingRect().center().x() + 50
            stY = startShape.boundingRect().center().y() - 100

            contP1 = QPointF(p1.x() - 30, p1.y() - 50)
            contP2 = QPointF(p2.x() + 30, p2.y() - 50)

            path = QPainterPath()
            path.moveTo(p1)
            path.cubicTo(contP1, contP2, p2)

            self.setPath(path)

        endX = p2.x()
        endY = p2.y()
        edgeLength = sqrt((endX - stX) ** 2 + (endY - stY) ** 2)
        if edgeLength != 0 and startNode != endNode:
            self.setLine(QLineF(p1, p2))
        else :
            pass

        self.drawHead(edgeLength, endX, endY, stX, stY, p2)

        self.setFlag(QGraphicsItem.ItemIsSelectable)
        

    def drawHead(self, edgeLength, endX, endY, stX, stY, p2):
        if self.editor.is_oriented:
            arrowLength = 12
            arrowWidth = 4
            if edgeLength != 0:
                xC = endX - arrowLength * (endX - stX) / edgeLength
                yC = endY - arrowLength * (endY - stY) / edgeLength

                xD = xC + arrowWidth * -(endY - stY) / edgeLength
                yD = yC + arrowWidth * (endX - stX) / edgeLength
                D = QPointF(xD.real, yD.real)

                xE = xC - arrowWidth * -(endY - stY) / edgeLength
                yE = yC - arrowWidth * (endX - stX) / edgeLength
                E = QPointF(xE.real, yE.real)
                self.head = QGraphicsPolygonItem(QPolygonF([D, E, p2]), parent=self)
                self.head.setFillRule(Qt.WindingFill)
                self.head.setPen(self.pen())
                self.head.setBrush(self.pen().brush())
        else:
            self.head = QGraphicsPolygonItem(QPolygonF([p2, p2, p2]), parent=self)
            self.head.setPen(self.pen())
            self.head.setBrush(self.pen().brush())
    
    def defineLine(self, style):
        if style == "dashed":
            self.setPen(QPen(QBrush(self.color, Qt.SolidPattern), 1, Qt.DashLine, Qt.SquareCap, Qt.BevelJoin))
        elif style == "dotted":
            self.setPen(QPen(QBrush(self.color, Qt.SolidPattern), 1, Qt.DotLine, Qt.SquareCap, Qt.BevelJoin))
        elif style == "bold" :
            self.setPen(QPen(QBrush(self.color, Qt.SolidPattern), 3, Qt.SolidLine, Qt.SquareCap, Qt.BevelJoin))
        else: 
            self.setPen(QPen(QBrush(self.color, Qt.SolidPattern), 1, Qt.SolidLine, Qt.SquareCap, Qt.BevelJoin))


    def mousePressEvent(self, e):
        """
        This method is called when the user press a mouse button

        @param e: the mouse event
        
        """
        modifiers = QApplication.keyboardModifiers()
        # unselect all the selected items only if it's a single left click or if it's a right click on a non-selected item
        if modifiers == Qt.NoModifier and (e.button() == Qt.LeftButton or ( e.button() == Qt.RightButton and self not in self.scene().selectedItems())):
            for item in self.scene().selectedItems():
                item.setSelected(False)
            self.setSelected(True)  

class GraphicsEdge(QGraphicsLineItem, GraphicsMainEdge):
    """
    A custom QGraphicsLineItem. It represent an edge between two different nodes

    """

    def __init__(self, startNode, endNode, editor, map_to_scene, label, color, form):
        """
        Constructor

        @param startNode: a GraphicsNode.
        @param endNode: a GraphicsNode. When directed, the arrow points to the node
        @param current_state: a reference to the model
        @param pair_model_node: a dictionary linking the view and the model
        @param label: the current name of this edge
        @param color: the edge color

        """

        super().__init__(startNode, endNode, editor, map_to_scene, label, color, form)
        super().defineLine(self.form)
        self.head.setPen(QPen(self.color))
        self.head.setBrush(self.pen().brush())
        
        
    def Update(self):
        """
        When a change occurs, this method refresh the edge

        """
        self.setPen(QPen(self.color))
        startShape = self.startNode.mapToScene(self.startNode.shape())
        endShape = self.endNode.mapToScene(self.endNode.shape())

        p1 = closestPointTo(endShape.boundingRect().center(), startShape)
        p2 = closestPointTo(startShape.boundingRect().center(), endShape)

        self.setLine(QLineF(p1, p2))

        stX = p1.x()
        stY = p1.y()
        endX = p2.x()
        endY = p2.y()
        edgeLength = sqrt((endX - stX) ** 2 + (endY - stY) ** 2)

        super.drawHead(edgeLength, endX, endY, stX, stY, p2)

    def mousePressEvent(self, e):
        """
        This method is called when the user press a mouse button

        @param e: the mouse event
        
        """
        modifiers = QApplication.keyboardModifiers()
        # unselect all the selected items only if it's a single left click or if it's a right click on a non-selected item
        if modifiers == Qt.NoModifier and (e.button() == Qt.LeftButton or ( e.button() == Qt.RightButton and self not in self.scene().selectedItems())):
            for item in self.scene().selectedItems():
                item.setSelected(False)
            self.setSelected(True)  

class GraphicsSemiEdge(QGraphicsLineItem, GraphicsMainEdge):
    """
    This class make a semi-edge. It's the  edge between the starting node and the mouse.
    It's a custom QgraphicsLineItem

    """

    def __init__(self, startNode, endNode, editor):
        """
        Constructor

        @param startNode: a GraphicsNode.
        @param endNode: a GraphicsNode. When directed, the arrow points to the node
        @param current_state: a reference to the model

        """
        super().__init__(startNode, endNode, editor, None, None, None, None)


    def Update(self, p2=None):
        """
        When a change occurs, this method refresh the semi-edge
        Called each time the user moves the mouse while keeps the left click pressed

        """
        if p2 != None:
            startShape = self.startNode.mapToScene(self.startNode.shape())

            self.endNode = p2
            p1 = closestPointTo(self.endNode, startShape)

            self.setLine(QLineF(p1, self.endNode))

            stX = p1.x()
            stY = p1.y()
            endX = self.endNode.x()
            endY = self.endNode.y()
            edgeLength = sqrt((endX - stX) ** 2 + (endY - stY) ** 2)


class GraphicsSelfEdge(QGraphicsPathItem, GraphicsMainEdge):
    isDirected = False

    def __init__(self, startNode, endNode, editor, map_to_scene, label, color, form):

        super().__init__(startNode, endNode, editor, map_to_scene, label, color, form)
        super().defineLine(self.form)
        self.head.setPen(QPen(self.color))
        self.head.setBrush(self.pen().brush())

    def obsUpdate(self):
        startShape = self.startNode.mapToScene(self.startNode.shape())
        endShape = self.endNode.mapToScene(self.endNode.shape())

        p1 = closestPointTo(startShape.boundingRect().center() - QPointF(50, 100), startShape)
        p2 = closestPointTo(startShape.boundingRect().center() - QPointF(-50, 100), startShape)
        mid = (p1 + p2) / 2

        sizeRect = self.textItem.boundingRect()
        self.textItem.setPos(mid.x() - sizeRect.width() / 2, mid.y() - sizeRect.height())

        stX = startShape.boundingRect().center().x() + 50
        stY = startShape.boundingRect().center().y() - 100
        endX = p2.x()
        endY = p2.y()
        edgeLength = sqrt((endX - stX) ** 2 + (endY - stY) ** 2)

        contP1 = QPointF(p1.x() - 30, p1.y() - 50)
        contP2 = QPointF(p2.x() + 30, p2.y() - 50)

        path = QPainterPath()
        path.moveTo(p1)
        path.cubicTo(contP1, contP2, p2)

        self.setPath(path)

        super().drawHead(edgeLength, endX, endY, stX, stY, p2)

    def getAttributs(self):
        attributs = ""
        attributs += 'color="' + self.color + '",'
        attributs += 'style="' + self.form + '"'
        return attributs
