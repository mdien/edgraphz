##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

from model.element import *


class Edge(Element):
    """
    An element that represent an Edge
    """

    def __init__(self, node_a, node_b, is_oriented, label, color, form, id, old_id):
        """
        Constructor

        :param node_A: when oriented, it's the starting node
        :type node_A: Node
        :param node_b: the second node to build an edge
        :type node_b: Node
        :param is_oriented: True for oriented
        :type is_oriented: boolean
        :param label: Two Edges with the same name is allowed
        :type label: String
        :param color: Color in hexa like #FFFFFF
        :type color: String
        :param form: Means the representation (line, dotted, dashed, ...)
        :type form: String

        """
        Element.__init__(self, label, color, form, id, old_id)
        self.node_a = node_a
        self.node_b = node_b
        self.is_oriented = is_oriented