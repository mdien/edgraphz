##Copyright 2019, 2019 DUBETTIER Adrien adriendub@yahoo.fr
##Copyright 2019, 2019 LECLERC Benjamin benjamin.leclerc7@gmail.com
##
##This file is part of EdGraphZ.
##
##    EdGraphZ is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    EdGraphZ is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with EdGraphZ.  If not, see <https://www.gnu.org/licenses/>.

from PyQt5.QtWidgets import QInputDialog
from PyQt5.QtGui import QBrush

class ConvertDOT:

    def __init__(self, window, editor):
        """
        Constructor

        :param window: The actual window (scene) of the Editor.
        :type window: MainWindow
        :param current_state: The actual state of the model of the editor
        :type current_state: Editor
        """
        self.window = window
        self.editor = editor

    def write_dot(self, path_file_to_save):
        """
        Use for write in a file all the information of the current state editor in a DOT format

        :param path_file_to_save: Path of the save file DOT
        :type path_file_to_save: String
        """
        with open(path_file_to_save, 'w') as out:
            if self.editor.is_oriented :
                firstLine = 'digraph G {'
            else :
                firstLine = 'graph G {'
            rankdir = ("TB", "BT", "LR", "RL")
            newrankdir, okpressed = QInputDialog.getItem(self.window, "rankdir", "choose rankdir :", rankdir, 0, False)
            if newrankdir and okpressed :
                for line in (firstLine, 'rankdir="' + newrankdir + '";'):
                    out.write('{}\n'.format(line))

            for node in self.editor.all_nodes.keys():
                id_node = node.id
                if node.old_id is not None:
                    id_node = node.old_id
                if isinstance(node.color, QBrush):
                    color = node.color.color().name()
                else :
                    color = node.color.name()
                try:
                    out.write('{} [ label="{}" shape="{}" color="{}" ];\n'.format(id_node, node.label,
                                                                                    node.form,
                                                                                    color))
                except:
                    print("DOT generation error, node related")
                    return
            for edge in self.editor.all_edges:
                print("poney")
                id_node_a = edge.node_a.id
                id_node_b = edge.node_b.id
                if edge.node_a.old_id is not None:
                    id_node_a = edge.node_a.old_id
                if edge.node_b.old_id is not None:
                    id_node_b = edge.node_b.old_id
                if self.editor.is_oriented:
                    extremity = '>'
                else :
                    extremity = '-'
                if isinstance(edge.color, QBrush):
                    color = edge.color.color().name()
                else :
                    color = edge.color.name()
                try:
                    out.write(
                        '{} -{} {} [ label="{}"  color="{}" style="{}" ];\n'.format(id_node_a, extremity, id_node_b,
                                                            edge.label, color, edge.form))
                except:
                    print("DOT generation error, edge related")
                    return

            
            out.write('}\n')
        out.close()

    def write(self):
        pass